//
//  GameScene.swift
//  RidiculousFishingGame
//
//  Created by Harman Kaur on 2019-10-24.
//  Copyright © 2019 Harman Kaur. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene{
    
    var fishingFlag : String = "false"
    var fishMoving:String = "left"
    var fishMoving1:String = "left"
    var fishMoving2:String = "left"
    var entities = [GKEntity]()
    var graphs = [String : GKGraph]()
    var fishingrod : SKSpriteNode!
    var goodFish : SKSpriteNode!
    var badFish : SKSpriteNode!
    var rareFish : SKSpriteNode!
    var fishArray : [SKSpriteNode] = []
    var fishScore: Int! = 0
    private var lastUpdateTime : TimeInterval = 0
    private var label : SKLabelNode?
    private var spinnyNode : SKShapeNode?
    var score:SKLabelNode!
    var ringtone = SKAction.playSoundFileNamed("watersound.mp3", waitForCompletion: false)
    
    override func didMove(to view: SKView) {
        run(ringtone)
        self.fishingrod = self.childNode(withName: "fishingrod") as! SKSpriteNode
        self.goodFish = self.childNode(withName: "goodfish") as! SKSpriteNode
        self.badFish = self.childNode(withName: "badfish") as! SKSpriteNode
        self.rareFish = self.childNode(withName: "rarefish") as! SKSpriteNode
        self.score = self.childNode(withName: "score") as! SKLabelNode
        score.text = String(0)

        
       
        
    }
    
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        // GET THE POSITION WHERE THE MOUSE WAS CLICKED
        // ---------------------------------------------
        
        let mouseTouch = touches.first
        if (mouseTouch == nil) {
            return
        }
        let location = mouseTouch!.location(in: self)
        
        // WHAT NODE DID THE PLAYER TOUCH
        // ----------------------------------------------
        let nodeTouched = atPoint(location).name
        //print("Player touched: \(nodeTouched)")
        
        
        // GAME LOGIC: Move player based on touch
        if (nodeTouched == "start") {
//            if let scene = SKScene(fileNamed: "GameScene"){
//                scene.scaleMode = .aspectFill
//
//                //showing transition on clicking start
//                self.view?.presentScene(scene,transition: SKTransition.flipHorizontal(withDuration: 3))

//            }
            fishingFlag = "true"
        }
        if(nodeTouched == "left"){
            fishingrod.position.x = fishingrod.position.x - 10
        }
        else if(nodeTouched == "right"){
            fishingrod.position.x = fishingrod.position.x + 10
        }
        
        
        
        
//        incrementing score +10 for badfish, +20 for good fish and +30 for rarefish
        if(nodeTouched == "badfish"){
            print("\(nodeTouched)")
            if(badFish.position.y > size.height/2 + 50){
                badFish.removeFromParent()
                fishScore = fishScore + 10
                score.text = "\(String(fishScore))"
            }
            
        }
        if(nodeTouched == "goodfish"){
            if(goodFish.position.y > size.height/2 + 50){
                goodFish.removeFromParent()
                fishScore = fishScore + 20
                score.text = "\(String(fishScore))"
            }
            
        }
        if(nodeTouched == "rarefish"){
            if(rareFish.position.y > size.height/2 + 50){
                rareFish.removeFromParent()
                fishScore = fishScore + 30
                score.text = "\(String(fishScore))"
            }
            
        }
        if (nodeTouched == "playAgain") {
            
            if let scene = SKScene(fileNamed: "GameScene"){
                
                scene.scaleMode = .aspectFill
            
                
                self.view?.presentScene(scene,transition: SKTransition.flipHorizontal(withDuration: 2.5))
                
            }
            
        }
        
        
    }
    
 
    
    
    override func update(_ currentTime: TimeInterval) {
        if(fishingFlag == "true"){
            fishingrod.position.y = fishingrod.position.y - 3
            
            
            
        }
        
        if(fishingFlag == "moveup"){
            
            fishingrod.position.y = fishingrod.position.y + 3
        }
        
        if(fishingrod.position.y < 0){
            fishingFlag = ""
        }
        
        
        if(fishingrod.position.y > size.height/2 + 50 ){
            fishingFlag = ""
            fishingrod.position.y = size.height/2 + 50
        }
        
        if(badFish.position.y > size.height/2 + 49 && badFish.position.y < size.height - 50 ){
            
            badFish.position.x = fishingrod.position.x
            badFish.position.y = badFish.position.y + 2
        }
        if(goodFish.position.y > size.height/2 + 49 && goodFish.position.y < size.height - 50){
            goodFish.position.x = fishingrod.position.x - 50
            goodFish.position.y = goodFish.position.y + 3
        }
        if(rareFish.position.y > size.height/2 + 49 && rareFish.position.y < size.height - 50){
            rareFish.position.x = fishingrod.position.x + 50
            rareFish.position.y = rareFish.position.y + 1
        }
        
        if(badFish.position.y > size.height - 50){
            badFish.position.x = fishingrod.position.x
            badFish.position.y = badFish.position.y
        }
        if(goodFish.position.y > size.height - 50){
            goodFish.position.x = fishingrod.position.x - 50
            goodFish.position.y = goodFish.position.y
        }
        if(rareFish.position.y > size.height - 50){
            rareFish.position.x = fishingrod.position.x + 50
            rareFish.position.y = rareFish.position.y
        }
        
        
        
        
        
        if(fishingrod.frame.intersects(goodFish.frame)){
            if(goodFish.position.y < size.height/2 + 49){
            goodFish.position.x = fishingrod.position.x
            goodFish.position.y = fishingrod.position.y
            fishingFlag = "moveup"
                fishArray.append(goodFish)
                goodFish.zPosition = 999
                print("\(goodFish.name)")
                
            }
        }
        if(fishingrod.frame.intersects(badFish.frame)){
            if(badFish.position.y < size.height/2 + 49){
            badFish.position.x = fishingrod.position.x
            badFish.position.y = fishingrod.position.y
            fishingFlag = "moveup"
            fishArray.append(badFish)
                badFish.zPosition = 999
                print("\(badFish.name)")
            }
        }
        if(fishingrod.frame.intersects(rareFish.frame)){
            if(rareFish.position.y < size.height/2 + 49){
            rareFish.position.x = fishingrod.position.x
            rareFish.position.y = fishingrod.position.y
            fishingFlag = "moveup"
            fishArray.append(rareFish)
                rareFish.zPosition = 999
                print("\(rareFish.name)")
            }
        }
        
        
        
        
        
        if(fishMoving == "left"){
            let facingLeft = SKAction.scaleX(to: 1, duration: 0)
            goodFish.run(facingLeft)
           goodFish.position.x = goodFish.position.x - 1
//            returing fish if touches screen
            if(goodFish.position.x < 0){
                fishMoving = "right"
            }
        }else {
            let facingRight = SKAction.scaleX(to: -1, duration: 0)
            goodFish.run(facingRight)
            goodFish.position.x = goodFish.position.x + 1
            //            returing fish if touches screen
            if(goodFish.position.x > size.width){
                fishMoving = "left"
            }
        }
        
        if(fishMoving1 == "left"){
            let facingLeft = SKAction.scaleX(to: -1, duration: 0)
            badFish.run(facingLeft)
            badFish.position.x = badFish.position.x - 2
            //            returing fish if touches screen
            if(badFish.position.x < 0){
                fishMoving1 = "right"
            }
        }else {
            let facingRight = SKAction.scaleX(to: 1, duration: 0)
            badFish.run(facingRight)
            badFish.position.x = badFish.position.x + 2
            //            returing fish if touches screen
            if(badFish.position.x > size.width){
                fishMoving1 = "left"
            }
        }
        
        if(fishMoving2 == "left"){
            let facingLeft = SKAction.scaleX(to: 1, duration: 0)
            rareFish.run(facingLeft)
            rareFish.position.x = rareFish.position.x - 2
            //            returing fish if touches screen
            if(rareFish.position.x < 0){
                fishMoving2 = "right"
            }
        }else {
            let facingRight = SKAction.scaleX(to: -1, duration: 0)
            rareFish.run(facingRight)
            rareFish.position.x = rareFish.position.x + 2
            //            returing fish if touches screen
            if(rareFish.position.x > size.width){
                fishMoving2 = "left"
            }
        }
        
        
        
        
//        if (goodFish.position.x < 0 || goodFish.position.x < size.width){
//
//        }
//        else if (goodFish.position.x > size.width || goodFish.position.x > 0) {
//            goodFish.position.x = goodFish.position.x - 10
//        }
//
//        badFish.position.x = badFish.position.x + 10
    }
}


